# Setting up Webserver
One of the most important parts of an Retro Hotel is the webserver.  
On this page there are several methods how one can spin up a webserver in no time.  


## Contents
- [Setting up Webserver](#setting-up-webserver)
  - [Contents](#contents)
  - [Apache on Linux](#apache-on-linux)
  - [Apache on Windows](#apache-on-windows)
  - [Sites on IIS (Windows)](#sites-on-iis-windows)
  - [Nginx on Linux](#nginx-on-linux)
  - [Nginx on Windows](#nginx-on-windows)

## Apache on Linux

## Apache on Windows

## Sites on IIS (Windows)

## Nginx on Linux

## Nginx on Windows